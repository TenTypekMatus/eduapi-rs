<h1 align="center">An unofficial API for Edupage ported to Rust</h1>

## Features
- Everything that's in Edupage
  - Grades
  - Timetables
  - Messages
  - Classrooms
  - And more...
## Installation
- Since this is a Rust library, you should be able to add it to your project by simply running `cargo add eduapi.rs`
## Examples
```rust
todo!();
```