pub mod cookiejar {
    use std::any::Any;

    pub trait CookieJar {
        fn set_cookie(cookie: Box<dyn Any>, value: &str, options: Box<dyn Any>) -> Box<dyn Any> {
            drop(cookie);
            options

        }
        fn construct(arge: Box<dyn Any>) -> Box<dyn Any> {
            arge
        }
    }
}