pub use std::any::Any;

/// Edupage API endpoint
pub enum Endpoints {
    GetUser,
    GetClassroom,
    Gcall,
    SignOnlineLesson,
    TimelineGetData,
    // EduTimeline
    TimelineGetReplies,
    TimelineGetCreatedItems,
    TimelineCreateItem,
    TimelineCreateConfirmation,
    TimelineCreateReply,
    TimelineFlagHW,
    TimelineUploadAttachment,
    // E-Lernen
    ElearningDataTest,
    ElearningTestResults,
    ElearningCardsData,
    GradesData,
    SessionPing,
}
/// Edupage needs this for some unknown reason
pub enum Gender {
    Male, // String::from
    Female, // same as above
}
/// Roles that does EduPage define
pub enum Role { // The original defines this enum as EntityType (path: ../original-api/src/enums.js)
    StudyPlan,
    Student,
    CustPlan,
    StudentOnly,
    StudClass,
    Teacher,
    All(Box<dyn Any>),
    Class,
    StudentAll,
    StudentOnlyAll,
    Admin,
    Parent,
}
/// Types of assigments that Edupage takes.
pub enum Assignments {
    Homework,
    EtestHw,
    BigExam,
    SmallExam,
    OralExam,
    ReportExam,
    Testing, // Like Monitor or Testing 5
    Test,
    Pexam,
    /// Etest stuff
    Etest,
    ETPrint,
    ETLesson,
    Lesson,
    Project,
    Result,
    Curriculum,
    Timeline,
}
/// Timeline on EduPage.
pub enum Timeline {
    Message,
    /// Message to the person which substitutes the lesson.
    Message2Substituter,
    NoticeBoard,
    GradeAnnouncement,
    Grade,
    Ball, // Note
    Hw,
    HwStudentState,
    ANote,
    ANoteReminder,
    Process,
    ProcessAdmin,
    StudentAbsent,
    Accident,
    Event,
    Timetable,
    Substitution,
    /// Canteen stuff (C = Canteen)
    CMenu,
    CCredit,
    CSuspendOrReinstateOrders,
    COpening,
    Survey,
    Plan,
    Settings,
    Album,
    News,
    /// Test stuff (T = Test, not a generic type)
    TAssignment,
    TResult,
    Chat,
    CheckIn,
    /// Consultations (C = Consultations)
    CMessage,
    Consultation,
    Payments,
    SignIn,
    Curriculum,
    CReminder,
    BlackBoard,
    StudentPickup,
    TtCloudGen, // GCP isn't private at all, 1984 hosting is.
    Confirmation,
    Contest
}
/// EduPage API status
pub enum ApiStatus<E> {
    Success,
    Fail(E),
}