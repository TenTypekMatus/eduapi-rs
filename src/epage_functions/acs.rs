pub mod parse {
    use anyhow::Result;
    use serde::Deserialize;
    #[derive(Deserialize)]
    pub struct Response<T> {
        /// School info
        edupage: Vec<String>,
        /// Info about logged in user
        logged_user: Vec<String>,
        /// Rights of the user
        rights_of_the_user: T,
        /// Timezone which they're in.
        tz: Vec<String>,
        /// Weekdays
        weekdays: Vec<i32>,
        /// Instance/server on which the user is on.
        server_addr: Vec<String>,
        /// Language of the server.
        language: Vec<String>,
        /// Country code of the school.
        ccode: Vec<String>,
        /// Turnover if you're American/British
        turnover: Vec<String>,
        /// Secure hash for communicating with EduPage servers
        gsechash: Vec<String>,
        /// GPID ID list.
        /// TODO: Check what GPID actually is.
        gpids: Vec<String>,
        /// First day of the week.
        /// Default: false
        fdotw: Vec<bool>,
    }

    use serde_json::*;
    pub fn json() -> Result<(), anyhow::Error> {
        let v: Value = from_str("")?;
    }
}
