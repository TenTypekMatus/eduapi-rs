pub trait LoginError {
    fn construct(&self, msg: &str, _: &Self) -> anyhow::Error {
        anyhow!("A login error happened: {}", msg)
    }
}
struct Error;
pub use anyhow::anyhow;
impl LoginError for Error {
    fn construct(&self, msg: &str, _: &Self) -> anyhow::Error {
        anyhow!("An login error occured: {}", msg)
    }
}
pub trait ParseError {
    fn construct(&self, msg: &str, _: &Self) -> anyhow::Error {
        anyhow!("A problem occured while parsing: {}", msg)
    }
}

impl ParseError for Error {
    fn construct(&self, msg: &str, _: &Self) -> anyhow::Error {
        anyhow!("{:?}", msg)
    }
}