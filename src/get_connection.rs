/// Set the timeout to 10 minutes
pub const SESSION_PING_INTERVAL_MS: i32 = 10 * 60 * 1000;