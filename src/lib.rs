pub mod cookie_scraper;
pub mod enumerations;
pub mod epage_functions;
pub mod exceptions;
///# Module for the holy website that everyone in Czechoslovakia know: **Edupage**
///## Some basic data from Edupage:
/// - School info
/// - Info 'bout logged in user
/// - Rights of the user
/// - TZ
/// - Weekend days
/// - The server itself (e.g. matus.edupage.org)
/// - Full name of school (e.g. Matthew's street 69)
/// - Locale of the school (e.g. sk_SK)
/// - Turnover (if you're American)
/// - Request hash
pub mod get_connection;
pub mod rawdata;
pub mod utils;
